# MS-Component

### Description

What does this microservice do

### API Documentation
Run **npm run start:dev** on local
- http://localhost:3000/api

### Container
This will spin up containers and run migration and seeders.
Containers does not have Postgres that is why POSTGRES_HOST is set to host.docker.internal.
Please see details below on how to spin Postgres container

Build image
```sh
docker compose build --no-cache
```

Run this image

```sh
docker compose up -d
```
Run migration and seeders
```sh
docker compose exec ms-profile-node sh -c "sh scripts/setup-dev.sh"
```


### Local Set up

- Copy environment variables and run script to configure your local

```bash
cp .env.example .env
sh sh scripts/setup-dev.sh
```

### Migration and Seeder

- How to create new migration

It will generate migration based on entity created

if table already exist, this won't work, this only applies to non existing tables

```bash
npm run typeorm:migration:generate "name_of_migration"

```

if you want to create alter migration

You will need to update the created migration file to reflect your intended changes
```bash
npm run typeorm -- migration:create "name_of_migration"

```

- How to create seeders

Copy one of the existing factory on **"src/factories"** and create a new factory file for your entity

Copy one of the exiting seeder **"src/seeds"** and create a new seeder file;

You can then run you seed as follows:
```bash
npm run typeorm:seed:run "seeder_class_name"

```

## To run the app with Node.js and Postgres (without Docker) in your local:

- run 'npm install'
- set up your Postgres database 
- rename .env.sample to .env and populate the required parameters
- run 'npm run start:dev'

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Setting Up Postgres Database via Docker

- Download and install 'Docker Desktop' (Mac/Windows)
- Download Docker official image for Postgres from docker hub repository 

```bash
$ docker pull postgres
```

- After downloading the image, check that if it is available to use:

```bash
$ docker images
```


#### 1. Create a folder in a known location for you

```bash
$ mkdir ${HOME}/postgres-data/
```

#### 2. run the postgres image
This will spin Postgres database container with default database "marketplace"

Connecting to your Postgres Database
  - via Host ( host: localhost:5432 )

```bash
$ docker run -d \
	--name db-postgres \
	-e POSTGRES_PASSWORD=YourPassword!  \
  -e POSTGRES_DB=marketplace
	-v ${HOME}/postgres-data/:/var/lib/postgresql/data \
        -p 5432:5432
        postgres
```


## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```