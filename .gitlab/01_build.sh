#!/bin/bash
## export timestamp for reporting
date -d "today" > ./timestamp.txt

[[ "$AWS_ACCESS_KEY_ID" ]] || { echo "Aws access key id doesn't exist in env variable. Please set it"; exit; }
[[ "$AWS_SECRET_ACCESS_KEY" ]] || { echo "Aws secret key id doesn't exist in env variable. Please set it"; exit; }
[[ "$AWS_DEFAULT_REGION" ]] || { echo "Aws default region doesn't exist in env variable. Please set it"; exit; }
[[ "$PROJECT_NAME" ]] || { echo "Project name  doesn't exist in env variable. Please set it"; exit; }

if   [ "$CI_COMMIT_REF_NAME" == "master" ]; then
  export ENVIRONMENT=prd
  export AWS_DEFAULT_REGION=ap-southeast-1
elif [ "$CI_COMMIT_REF_NAME" == "staging" ]; then
  export ENVIRONMENT=stg
  export AWS_DEFAULT_REGION=ap-southeast-1
elif [ "$CI_COMMIT_REF_NAME" == "develop" ]; then
  export ENVIRONMENT=dev
  export AWS_DEFAULT_REGION=us-east-1
else
  export ENVIRONMENT=dev
  export AWS_DEFAULT_REGION=us-east-1
fi

echo "Update submodule.."
git --version
git submodule update --init --recursive --remote
git rev-parse --show-toplevel
git ls-files

export ENVVAR=$(aws ssm get-parameters  --region ${AWS_DEFAULT_REGION} --with-decryption --names "${PROJECT_NAME}-${ENVIRONMENT}-product-service" | jq -r '.Parameters[]  | .Value')
echo "Parsed variables on parameter store: $ENVVAR"
echo "$ENVVAR" > .env

echo "CI_COMMIT_TAG is $CI_COMMIT_TAG"

export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --output text --query Account)
COMMIT_TAG=${CI_COMMIT_SHA:0:8}
ECR_REPO_NAME=$PROJECT_NAME-product-service
ECR_URL="${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com"
ECR_PATH="$ECR_URL/$ECR_REPO_NAME"

aws ecr describe-repositories --region ${AWS_DEFAULT_REGION} --repository-names $ECR_REPO_NAME 2>&1 > /dev/null
status=$?
if [[ ! "${status}" -eq 0 ]]; then
    echo "creating ecr repository '$ECR_REPO_NAME' ... "
    aws ecr create-repository --region ${AWS_DEFAULT_REGION} --repository-name $ECR_REPO_NAME
fi
cp .gitlab/task-definition.json .

docker build -t registry.ubx.ph/$CI_PROJECT_PATH/$PROJECT_NAME:${CI_COMMIT_SHA:0:8} .
docker push registry.ubx.ph/$CI_PROJECT_PATH/$PROJECT_NAME:${CI_COMMIT_SHA:0:8}
