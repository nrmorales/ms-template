#!/bin/bash

[[ "$AWS_ACCESS_KEY_ID" ]] || { echo "Aws access key id doesn't exist in env variable. Please set it"; exit; }
[[ "$AWS_SECRET_ACCESS_KEY" ]] || { echo "Aws secret key id doesn't exist in env variable. Please set it"; exit; }
[[ "$AWS_DEFAULT_REGION" ]] || { echo "Aws default region doesn't exist in env variable. Please set it"; exit; }
[[ "$PROJECT_NAME" ]] || { echo "Project name  doesn't exist in env variable. Please set it"; exit; }

# Use environment depending commit branch
if [ "$CI_COMMIT_REF_NAME" == "develop" ]; then
  export ENVIRONMENT=dev
  export AWS_DEFAULT_REGION=us-east-1
  export DD=false
fi
if [ "$CI_COMMIT_REF_NAME" == "staging" ]; then
  export ENVIRONMENT=stg
  export AWS_DEFAULT_REGION=ap-southeast-1
  export DD=false
fi
if [ "$CI_COMMIT_REF_NAME" == "master" ]; then
  export ENVIRONMENT=prd
  export AWS_DEFAULT_REGION=ap-southeast-1
  export DD=true
fi

export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --output text --query Account)
ECR_REPO_NAME=$PROJECT_NAME-product-service
COMMIT_TAG=${CI_COMMIT_SHA:0:8}
ECR_URL="${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_DEFAULT_REGION}.amazonaws.com"
AWS_REGISTRY_IMAGE="$ECR_URL/$ECR_REPO_NAME"

#Pull Release build from Gitlab
docker pull registry.ubx.ph/$CI_PROJECT_PATH/$PROJECT_NAME:${CI_COMMIT_SHA:0:8}

#Tag for deployment and Push to Gitlab (for backup purposes)
docker tag registry.ubx.ph/$CI_PROJECT_PATH/$PROJECT_NAME:${CI_COMMIT_SHA:0:8} registry.ubx.ph/$CI_PROJECT_PATH/$PROJECT_NAME:$CI_COMMIT_REF_NAME
docker push registry.ubx.ph/$CI_PROJECT_PATH/$PROJECT_NAME:$CI_COMMIT_REF_NAME

#Tag for deployment and Push to Gitlab (for deployment purposes)
$(aws ecr get-login --no-include-email --region $AWS_DEFAULT_REGION | tr -d '\r')
docker tag registry.ubx.ph/$CI_PROJECT_PATH/$PROJECT_NAME:${CI_COMMIT_SHA:0:8} $AWS_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
docker push $AWS_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

#Prepare app.json
cp .gitlab/*.json .
sed -i "s#CHANGEENV#$ENVIRONMENT#g" *.json
sed -i "s#AWSREGION#$AWS_DEFAULT_REGION#g" *.json
sed -i "s#AWSACCOUNT#$AWS_ACCOUNT_ID#g" *.json
sed -i "s#COMMITTAG#$CI_COMMIT_REF_NAME#g" *.json
sed -i "s#DDRUN#$DD#g" *.json

# Deploy API container
echo ""
echo "#############################################################"
echo "# Deploying Latest version of $CI_COMMIT_REF_NAME api container #"
echo "#############################################################"
echo ""
aws ecs register-task-definition --family product-service-task-def --requires-compatibilities FARGATE --cpu 256 --memory 512 --cli-input-json file://task-definition.json
aws ecs update-service --cluster $PROJECT_NAME-$ENVIRONMENT --service product-service --task-definition product-service-task-def --region $AWS_DEFAULT_REGION


# Wait
echo ""
echo "Waiting for rolling update to complete..."
echo ""
aws ecs wait services-stable --cluster $PROJECT_NAME-$ENVIRONMENT --service product-service
echo ""
echo "Rolling Update Complete!"
echo ""
