import { configService } from './config.service';
import fs = require('fs');

const TypeOrmConfig = {
  ...configService.getTypeOrmConfig(),
  seeds: ['src/seeds/**/*.ts'],
  factories: ['src/factories/**/*.ts'],
};

fs.writeFileSync('ormconfig.json', JSON.stringify(TypeOrmConfig, null, 2));
