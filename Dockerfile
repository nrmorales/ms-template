# Builder image
FROM node:14-alpine AS builder

WORKDIR /package
COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

# Final image
FROM node:14-alpine AS final

WORKDIR /app
COPY package*.json ./

# Install only dependencies
RUN npm install --only=production

COPY . .

COPY --from=builder /package/dist ./dist

# Execute
CMD ["npm", "run", "start:prod"]
