import { Test, TestingModule } from '@nestjs/testing';
import { HttpService, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { configService } from '../src/config/config.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MerchantModule } from '../src/merchant/merchant.module';
import { CustomerModule } from '../src/customer/customer.module';
import { AppController } from 'src/app.controller';
import { AppService } from 'src/app.service';
import { MerchantService } from 'src/merchant/merchant.service';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let httpService: HttpService;

  beforeAll(async () => {
    const testAppModule: TestingModule = await Test.createTestingModule({
      imports: [AppModule, MerchantModule],
      providers: [MerchantService],
    }).compile();

    app = testAppModule.createNestApplication();
    httpService = testAppModule.get<HttpService>(HttpService);
    await app.init();
 });

  // beforeEach(async () => {
  //   const moduleFixture: TestingModule = await Test.createTestingModule({
  //     imports: [
  //       TypeOrmModule.forRoot(configService.getTypeOrmConfig()),
  //       AppModule,
  //       MerchantModule,
  //       CustomerModule,
  //     ],
  //     controllers: [AppController],
  //     providers: [AppService],
  //   }).compile();

  //   app = moduleFixture.createNestApplication();
  //   await app.init();
  // });

  // it('/ (GET)', () => {
  //   return request(app.getHttpServer())
  //     .get('/')
  //     .expect(200)
  //     .expect('Hello World!');
  // });
});
