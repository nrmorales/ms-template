
echo 'running migration...' &&
npm run typeorm:migration:run &&
echo 'running seeders...'
npm run typeorm:seed:run createCountries &&
npm run typeorm:seed:run createCurrencies &&
npm run typeorm:seed:run createRegion &&
npm run typeorm:seed:run createProvinces &&
npm run typeorm:seed:run createCities &&
echo 'running dummy seeders...' &&
npm run typeorm:seed:run createAddress &&
npm run typeorm:seed:run createBank &&
npm run typeorm:seed:run createCompany &&
npm run typeorm:seed:run createMerchant &&
echo 'done setup'